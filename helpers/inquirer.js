const inquirer = require('inquirer')
require('colors')

const questions = [
  {
    type: 'list',
    name: 'option',
    message: 'What do you wish to do?',
    choices: [
      {
        value: '1',
        name: `${'1'.green}. Create task`
      },
      {
        value: '2',
        name: `${'2'.green}. List tasks`
      },
      {
        value: '3',
        name: `${'3'.green}. List completed tasks`
      },
      {
        value: '4',
        name: `${'4'.green}. List pending tasks`
      },
      {
        value: '5',
        name: `${'5'.green}. Complete tasks`
      },
      {
        value: '6',
        name: `${'6'.green}. Remove task`
      },
      {
        value: '0',
        name: `${'0'.green}. Exit`
      }
    ]
  }
]

const inquirerMenu = async () => {
  console.clear()

  console.log('Select option:'.green + '\n')

  const { option } = await inquirer.prompt(questions)

  return option
}

const pause = async () => {
  console.log('\n')

  await inquirer.prompt([{
    type: 'input',
    name: 'enter',
    message: `\nPress ${'ENTER'.green} to continue\n`
  }])
}

const readInput = async (message) => {
  const question = [
    {
      type: 'input',
      name: 'description',
      message,
      validate (value) {
        if (value.length === 0) {
          return 'Please, enter a value'
        }

        return true
      }
    }
  ]

  const { description } = await inquirer.prompt(question)
  return description
}

const listTasksToDelete = async (tasks = []) => {
  const choices = tasks.map((task, i) => {
    const index = `${i + 1}`.green

    return {
      value: task.id,
      name: `${index} ${task.description}`
    }
  })

  choices.unshift({
    value: '0',
    name: '0.'.green + 'Cancelar'
  })

  const questions = [
    {
      type: 'list',
      name: 'id',
      message: 'Delete',
      choices
    }
  ]
  const { id } = await inquirer.prompt(questions)

  return id
}

const listTasksToComplete = async (tasks = []) => {
  const choices = tasks.map((task, i) => {
    const index = `${i + 1}`.green

    return {
      value: task.id,
      name: `${index} ${task.description}`,
      checked: !!(task.completedIn)
    }
  })

  const question = [
    {
      type: 'checkbox',
      name: 'ids',
      message: 'Select',
      choices
    }
  ]
  const { ids } = await inquirer.prompt(question)

  return ids
}

const confirmTaskDelete = async (message) => {
  const question = [
    {
      type: 'confirm',
      name: 'ok',
      message
    }
  ]

  const { ok } = await inquirer.prompt(question)

  return ok
}

module.exports = {
  inquirerMenu,
  pause,
  readInput,
  listTasksToDelete,
  confirmTaskDelete,
  listTasksToComplete
}
