const { v4: uuidV4 } = require('uuid')

class Task {
  id = ''
  description = ''
  completedIn = null // Si contiene una fecha quiere decir que está completada.

  constructor (description) {
    this.id = uuidV4()
    this.description = description
  }
}

module.exports = Task
