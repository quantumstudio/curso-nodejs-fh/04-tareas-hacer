require('colors')

const Task = require('./task')

/**
 * _list:
 * {uuid-122312313123-3: {id: 12, description: 'Tarea', completedIn: '2021-05-07'}}
 */
class Tasks {
  _list = {}

  get list () {
    const list = []
    Object.keys(this._list).forEach((key) => {
      const task = this._list[key]
      list.push(task)
    })

    return list
  }

  constructor () {
    this._list = {}
  }

  createTask (description = '') {
    const task = new Task(description)

    this._list[task.id] = task
  }

  loadTasksFromDB (tasks = []) {
    tasks.forEach((task) => {
      this._list[task.id] = task
    })
  }

  listTasks () {
    const tasks = this.list

    if (tasks.length > 0) {
      tasks.forEach((task, index) => {
        const taskNumber = `${index + 1}`.green
        const { description, completedIn } = task
        const status = completedIn ? 'Completada'.green : 'Pendiente'.red

        console.log(`${taskNumber}. ${description} :: ${status}`)
      })
    } else {
      console.log('No tasks 😥')
    }
  }

  listPendingTasks () {
    const tasks = this.list

    if (tasks.length > 0) {
      tasks.forEach((task, index) => {
        const taskNumber = `${index + 1}`.green
        const { description, completedIn } = task

        if (!completedIn) {
          console.log(`${taskNumber}. ${description}`)
        }
      })
    } else {
      console.log('No pending tasks 👌')
    }
  }

  listCompletedTasks () {
    const tasks = this.list

    if (tasks.length > 0) {
      tasks.forEach((task, index) => {
        const taskNumber = `${index + 1}`.green
        const { description, completedIn } = task

        if (completedIn) {
          console.log(`${taskNumber}. ${description} :: ${completedIn.green}`)
        }
      })
    } else {
      console.log('No completed tasks')
    }
  }

  deleteTask (id = '') {
    if (this._list[id]) {
      delete this._list[id]
    }
  }

  toggleCompleteTasks (ids = []) {
    ids.forEach((id) => {
      const task = this._list[id]

      if (!task.completedIn) {
        task.completedIn = new Date().toISOString()
      }
    })

    this.list.forEach((task) => {
      if (!ids.includes(task.id)) {
        this._list[task.id].completedIn = null
      }
    })
  }
}

module.exports = Tasks
