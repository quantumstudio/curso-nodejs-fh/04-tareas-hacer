require('colors')

const { inquirerMenu, pause, readInput, listTasksToDelete, confirmTaskDelete, listTasksToComplete } = require('./helpers/inquirer')
const { saveDB, readDB } = require('./helpers/saveFile')
const Tasks = require('./models/tasks')

console.clear()

const main = async () => {
  let opt = ''
  const tasks = new Tasks()

  const tasksDB = readDB()

  if (tasksDB) {
    // Set tasks
    tasks.loadTasksFromDB(tasksDB)
  }

  do {
    opt = await inquirerMenu()

    switch (opt) {
      case '1': {
        // create task
        const description = await readInput('Description:')
        tasks.createTask(description)

        break
      }

      case '2': {
        // List tasks.
        tasks.listTasks()
        break
      }

      case '3': {
        // List completed tasks.
        tasks.listCompletedTasks()
        break
      }

      case '4': {
        // List pending tasks.
        tasks.listPendingTasks()
        break
      }

      case '5': {
        // Complete tasks.
        const ids = await listTasksToComplete(tasks.list)
        tasks.toggleCompleteTasks(ids)
        break
      }

      case '6': {
        // Delete task.
        const id = await listTasksToDelete(tasks.list)

        if (id !== '0') {
          const confirm = await confirmTaskDelete('Are you sure?')

          if (confirm) {
            tasks.deleteTask(id)
            console.log('Task deleted ✔️')
          }
        }
        break
      }

      default:
        break
    }

    saveDB(tasks.list)

    if (opt !== '0') {
      await pause()
    }
  } while (opt !== '0')
}

main()
